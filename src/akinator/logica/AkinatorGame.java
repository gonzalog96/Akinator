package akinator.logica;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public class AkinatorGame 
{
	// variables.
	private String paisUsuario;
	private HashMap<String, Pais> paises;
	
	private Random genPregsAleat;
	private ArrayList<String> preguntas; // algo similar deberia pasar con los paises.
	private int cantOpcionesRestantes;
	
	private final String[] elecciones = {"S�", "Probablemente", "Puede ser", "No"};
	
	// constructor.
	public AkinatorGame(String pais)
	{
		paises = new HashMap<>();
		cargarPaises();
		chequearPaisValido(pais); // chequear si el pais es valido o no.
		
		paisUsuario = pais;
		genPregsAleat = new Random();
		cargarPreguntas();
	}
	
	// este metodo carga los paises (en este caso, de latinoamerica).
	/** deberia cargar esto desde una bd -opcional - */
	public void cargarPaises()
	{
		// nombrePais, ubicacion, poblacion, esIsla, tipoDeBandera.
		Pais argentina = new Pais("Argentina", "Sur", 1, 17000000, false, Bandera.DOS_COLORES_CON_SOL);
		Pais venezuela = new Pais("Venezuela", "Sur", 1, 17000000, false, Bandera.TRES_COLORES_CON_ESTRELLAS);
		Pais chile =  new Pais("Chile", "Sur", 1, 25000000, false, Bandera.TRES_COLORES_CON_ESTRELLAS);
		Pais mexico = new Pais("Mexico", "Centro", 2, 25000000, false, Bandera.TRES_COLORES_SIN_ESTRELLAS);
		
		paises.put(argentina.dameNombre(), argentina);
		paises.put(venezuela.dameNombre(), venezuela);
		paises.put(chile.dameNombre(), chile);
		paises.put(mexico.dameNombre(), mexico);
	}
	
	// este m�todo carga las preguntas.
	// deber�an obtenerse desde una base de datos.
	public void cargarPreguntas()
	{
		preguntas = new ArrayList<>(Arrays.asList(
		"�Est� ubicado en Am�rica del Sur?",
		"�Su poblaci�n habla m�s de un idioma?",
		"�Tiene menos de 10.000.000 habitantes?",
		"�Es una isla?",
		"�Su nombre tiene entre 2 y 4 caracteres?",
		"�La primer letra es una vocal?",
		"�La �ltima letra es una consonante?",
		
		"�La bandera posee dos colores, SIN estrellas en alguna parte?",
		"�La banera posee dos colores, CON estrellas en alguna parte?",
		"�La bandera posee dos colores, SIN ninguna inscripci�n (sol)? en alguna parte?",
		"�La bandera posee dos colores, CON inscripci�n (sol) en alguna parte?",
		
		"�La bandera posee tres colores, SIN estrellas en alguna parte?",
		"�La bandera posee tres colores, CON estrellas en alguna parte?",
		"�La bandera posee tres colores, SIN inscripci�n (sol) en alguna parte?",
		"La bandera posee tres colores, CON inscripci�n (sol) en alguna parte?"));
		
		cantOpcionesRestantes = preguntas.size();
	}
	
	// este metodo selecciona (al azar), alguna de las preguntas ya cargadas en el sistema.
	// notar que si esa pregunta se selecciona, al mismo tiempo se elimina de la lista (no se pueden repetir preguntas).
	public String seleccionarPregunta()
	{
		int indicePreg = genPregsAleat.nextInt(preguntas.size());
		String preguntaEleg = preguntas.get(indicePreg);
		
		preguntas.remove(indicePreg);
		cantOpcionesRestantes--;
		
		return preguntaEleg;
	}
	
	private void chequearPaisValido(String pais)
	{
		if (!paises.containsKey(pais))
			throw new IllegalArgumentException("El pais ingresado NO existe o es inv�lido: recuerde respetar may�sculas y min�sculas!");
	}
	
	// (...) INC.
}