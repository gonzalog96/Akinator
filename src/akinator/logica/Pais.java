package akinator.logica;

/*
 * TAD interno Pais
 * -> La modelizaci�n de los pa�ses rige solamente para aquellos dentro de latinoam�rica.
 * En total son 21 pa�ses. P = {Mexico, Argentina, Cuba, Bolivia, Chile, ..., Venezuela}.
 */
class Pais 
{
	// atributo general.
	private String nombre;
	
	// atributos de seleccion: se usaran para formular las preguntas del akinator.
	private String ubicacion;
	private int cantIdiomas;
	private int cantidadDePoblacion;
	private boolean esIsla;
	private int cantLetras;
	private char primeraLetra;
	private char ultimaLetra;
	private Bandera coloresDeBandera;
	
	// constructor
	public Pais(String n, String u, int i, int cantPob, boolean isla, Bandera tipoDeBandera)
	{
		nombre = n;
		
		ubicacion = u;
		cantIdiomas = i;
		cantidadDePoblacion = cantPob;
		esIsla = isla;
		cantLetras = n.length();
		primeraLetra = n.charAt(0);
		ultimaLetra = n.charAt(n.length()-1);
		coloresDeBandera = tipoDeBandera;
	}
	
	// getters.
	public String dameNombre()
	{
		return nombre;
	}
	
	public String dameUbicacion()
	{
		return ubicacion;
	}
	
	public int dameCantIdiomas()
	{
		return cantIdiomas;
	}
	
	public int damePoblacion()
	{
		return cantidadDePoblacion;
	}
	
	public boolean dameSiEsIsla()
	{
		return esIsla;
	}
	
	public int dameCantDeLetras()
	{
		return cantLetras;
	}
	
	public char damePrimeraLetra()
	{
		return primeraLetra;
	}
	
	public char dameUltimaLetra()
	{
		return ultimaLetra;
	}
	
	public Bandera dameTipoDeBandera()
	{
		return coloresDeBandera;
	}
}